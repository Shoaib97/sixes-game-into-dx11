#include "Dice.h"


void Dice::init()
//reset the random number generator from current system time
{
	rng_.seed();
}
int Dice::getFace() const
//get the value of the dice face
{
	return face_;
}
void Dice::roll()
//roll the dice
{
	face_ = rng_.getRandomValue(6);
}