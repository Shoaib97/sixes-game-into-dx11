#pragma once
#include "RandomNumberGenerator.h"



//--------Dice class (version 2)
class Dice
{
public:
	void init();
	int getFace() const;
	void roll();
private:
	int face_;		//the number on the dice
	RandomNumberGenerator rng_; 	//the private random number generator
};