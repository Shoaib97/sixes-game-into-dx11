#pragma once
//#include<stdio.h>
#include<ctime>
#include <stdlib.h>

//--------RandomNumberGenerator class
class RandomNumberGenerator
{
public:
	void seed() const;
	int getRandomValue(int) const;
};