#include "Game.h"
#include<string>
#include "GameData.h"
#include "Player.h"

using namespace std;


void Game::init() {
	dice1_.init();
	dice2_.init();
}
int Game::play(int goes, GameData& gdata_, Score& score) {
	goes -= 50;
	score.init();
	float x = 20;
	float y = 40;
	float inc = 20;
	for (int i = 1; i <= goes; i++)
	{
		dice1_.roll();
		dice2_.roll();
		string mssg = "  \nIn try no " + to_string(i) + " the dice values are: " + to_string(dice1_.getFace()) + " & " + to_string(dice2_.getFace()) + ".";
		//gdata_.text.setString(mssg);

		if (dice1_.getFace() == dice2_.getFace())
		{
			score.updateAmount(dice1_.getFace());
		}
		else
			if ((dice1_.getFace() == 6) || (dice2_.getFace() == 6))
				score.updateAmount(1);
		mssg += " Therefore, the current score is " + to_string(score.getAmount()) + ".";
		//gdata_.text.setString(mssg);
		//gdata_.text.setPosition(x, y += inc);
		//gdata_.window.draw(gdata_.text);
	}
	return score.getAmount();
}

