#pragma once
#include "Score.h"
#include<string>

using namespace std;

class Player {
private:
	string name_;
	Score score_;
	int throws_;
public:
	void init(const string& name);
	void updateScoreAmount(int a);
	string getName() const;
	int getScoreAmount() const;

};