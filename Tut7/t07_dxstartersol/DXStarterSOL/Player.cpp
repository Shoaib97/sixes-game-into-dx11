#include "Player.h"




void Player::init(const string& name) {
	score_.init();
	name_ = name;
}

void Player::updateScoreAmount(int a) {
	score_.updateAmount(a);
}
string Player::getName() const {
	return name_;
}
int Player::getScoreAmount() const {
	return score_.getAmount();
}