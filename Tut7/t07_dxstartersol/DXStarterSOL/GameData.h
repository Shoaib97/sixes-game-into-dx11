#pragma once
#include<string>
#include "Score.h"
#include "Dice.h"

struct GameData
{
	enum class Gamemode
	{
		Player_Name,
		Goes_No,
		Exit,
		Play,
		Replay
	};
	Gamemode mode = Gamemode::Player_Name;

	
	//keys to use
	enum
	{
		Enter_Key = 13,		//ASCII value
		Escape_Key = 27,		//ASCII value
		Replay_Key = 114
	};

	enum
	{
		Print_X = 20,
		Print_Y = 40,
		Print_Inc = 20,
	};

	int finalScore;
	int goes;


	Dice dice1_;
	Dice dice2_;

	std::string name;
	std::string message;
	std::string message2;
	std::string message3;
	std::string message4;
	std::string message5;
	std::string message6;


};
