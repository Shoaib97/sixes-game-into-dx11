#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <iomanip>

#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include "SimpleMath.h"
#include "SpriteFont.h"
#include "GameData.h"
#include "Score.h"



using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;


DirectX::SpriteFont *gpFont = nullptr;
DirectX::SpriteBatch *gpSpriteBatch = nullptr;
float gResTimer = 0;
int gFrameCounter = 0;
GameData gamedata;
Score score;


void InitGame(MyD3D& d3d)
{
	

	gpSpriteBatch = new SpriteBatch(&d3d.GetDeviceCtx());
	assert(gpSpriteBatch);

	gpFont = new SpriteFont(&d3d.GetDevice(), L"../bin/data/comicSansMS.spritefont");
	assert(gpFont);

}


//any memory or resources we made need releasing at the end
void ReleaseGame()
{
	delete gpFont;
	gpFont = nullptr;

	delete gpSpriteBatch;
	gpSpriteBatch = nullptr;
}

//called over and over, use it to update game logic
void Update(float dTime, MyD3D& d3d)
{
	switch (gamedata.mode)
	{
	case GameData::Gamemode::Player_Name:	//Player enters their name
		gamedata.message = "Please enter your name: ";
		gamedata.message += gamedata.name;
		break;
	case GameData::Gamemode::Goes_No:		//Player enters the amount of timnes he want to roll the dice
		gamedata.message2 = "How many goes do you want: ";
		gamedata.message2 += gamedata.goes;
		break;
	case GameData::Gamemode::Play:			//The game plays and no of goes is displayed with the total score at the end
		gamedata.goes -= 50;
		
		for (int i = 1; i <= gamedata.goes; i++)
		{
			gamedata.dice1_.roll();
			gamedata.dice2_.roll();
			//std::string mssg = "  \nIn try no " + to_string(i) + " the dice values are: " + to_string(gamedata.dice1_.getFace()) + " & " + to_string(gamedata.dice2_.getFace()) + ".";
			//gdata_.text.setString(mssg);

			if (gamedata.dice1_.getFace() == gamedata.dice2_.getFace())
			{
				score.updateAmount(gamedata.dice1_.getFace());
				gamedata.finalScore = score.amount_;
			}
			else
				if ((gamedata.dice1_.getFace() == 6) || (gamedata.dice2_.getFace() == 6))
					score.updateAmount(1);
					gamedata.finalScore = score.amount_;
			gamedata.message3 += "  \nIn try no " + to_string(i) + " the dice values are: " + to_string(gamedata.dice1_.getFace()) + " & " + to_string(gamedata.dice2_.getFace()) + ". Therefore, the current score is " + to_string(score.getAmount()) + ".";
			gamedata.message4 = "\n\nPress enter to obtain your final score";
		}
		break;
	case GameData::Gamemode::Exit:		//Exiting the game after player presses enter
		gamedata.message5 = "\n\n" + gamedata.name + "'s final score is " + to_string(gamedata.finalScore);
		gamedata.message6 = "\n\n\nPress esc or q to quit the game";
		break;
	
	default:
		assert(false);
	}
}

//called over and over, use it to render things
void Render(float dTime, MyD3D& d3d)
{
	float x = GameData::Print_X;
	float y = GameData::Print_Y;
	float inc = GameData::Print_Inc;

	WinUtil& wu = WinUtil::Get();
	
	d3d.BeginRender(Colours::Black);
	gpSpriteBatch->Begin();

	Vector2 scrn{ (float)wu.GetData().clientWidth, (float)wu.GetData().clientHeight };
	Vector2 pos = Vector2(0, 0);	//Creating a vector for the position of text on screen 

	pos = { x, y += inc };
	gpFont->DrawString(gpSpriteBatch, gamedata.message.c_str(), pos);

	pos = { x, y += inc };
	gpFont->DrawString(gpSpriteBatch, gamedata.message2.c_str(), pos);

	pos = { x, y += inc };
	gpFont->DrawString(gpSpriteBatch, gamedata.message3.c_str(), pos);
	
	pos = { x, y += inc + 150 };
	gpFont->DrawString(gpSpriteBatch, gamedata.message4.c_str(), pos);

	pos = { x, y += inc + 150 };
	gpFont->DrawString(gpSpriteBatch, gamedata.message5.c_str(), pos);

	pos = { x, y += inc };
	gpFont->DrawString(gpSpriteBatch, gamedata.message6.c_str(), pos);
  

	gpSpriteBatch->End();
	d3d.EndRender();
}

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	gResTimer = GetClock() + 2;
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			break;

		case 0x0d:	//processes a carriage return
			if (gamedata.mode == GameData::Gamemode::Player_Name)
			{
				gamedata.mode = GameData::Gamemode::Goes_No;
			}
			else if (gamedata.mode == GameData::Gamemode::Goes_No)
			{
				gamedata.mode = GameData::Gamemode::Play;
			}

			else if (gamedata.mode == GameData::Gamemode::Play)
			{
				gamedata.mode = GameData::Gamemode::Exit;
			}
			break;
		
		default:

			if (isalpha(wParam))
			{
				gamedata.name += wParam;
			}
			if (isdigit(wParam))
			{
				gamedata.goes += wParam;
			}
			break;
			return 0;
		}
	}
	
	

	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(1024), h(768);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Sixes Game", MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
	InitGame(d3d);

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			Update(dTime, d3d);
			Render(dTime, d3d);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
		gFrameCounter++;
	}

	ReleaseGame();
	d3d.ReleaseD3D(true);	
	return 0;
}